﻿#include "pch.h"
#include <iostream>

struct Employee {

    short* id;
    int* age;
    double salary;
    char* name;
};

int main()
{
    Employee* p_struct;

    std::cout << "The size of struct Employee is\n" << sizeof(Employee) << "\n";

    return 0;
}